from django.db import models


class Cart(models.Model):
    """
    Cart model.
    """

    id = models.BigAutoField(primary_key=True)


class Item(models.Model):
    """
    Item model.
    """

    id = models.BigAutoField(primary_key=True)
    cart = models.ForeignKey(Cart, on_delete=models.CASCADE, related_name="items")  # external_id

    name = models.CharField(max_length=100, null=True)
    value = models.IntegerField(null=True)  # in cents

    class Meta:
        indexes = [
            models.Index(fields=["cart"]),
        ]

    def __str__(self):
        return f"{self.name} ({self.id})"


""" 
I followed the task description, but normally I would have an additional, intermediate model between Cart and Item to
connect them in M2M relationship, and the ItemCreateAPIView accept not {"name": str, "value": int} object(s), but rather
a previously fetched Item.id(s). In such scenario Items would be pre-defined by admin, returned as a list in a separate
view, from which user would be able to choose before passing them back from front-end to back-end's.
"""
