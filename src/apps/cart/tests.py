from http.cookies import SimpleCookie
from typing import Generator

from django.conf import settings
from rest_framework import reverse, status

import pytest

from apps.cart.models import Cart, Item


@pytest.fixture
def fixture__cart() -> Generator[Cart, None, None]:
    cart = Cart.objects.create()
    yield cart


class ItemTest:

    @pytest.mark.django_db
    @pytest.mark.parametrize(
        "item_data",
        ([{"name": "jeans", "value": 15}], [{"name": "hoodie", "value": 500}]),
    )
    def test__integration__add_item_to_cart__cart_does_not_exist__success(
        self,
        api_client,
        item_data,
    ):
        assert not Cart.objects.filter().first()
        assert not Item.objects.filter().first()

        response_create = api_client().post(
            reverse.reverse("item"),
            data=item_data,
            format="json",
        )

        expected_db_item = item_data[0]

        assert response_create.status_code == status.HTTP_204_NO_CONTENT
        assert Cart.objects.filter().first()
        assert Item.objects.get(**expected_db_item)


    @pytest.mark.django_db
    @pytest.mark.parametrize(
        "item_data",
        ([{"name": "jeans", "value": 15}], [{"name": "hoodie", "value": 500}]),
    )
    def test__integration__add_item_to_cart__cart_does_exist__no_cookie__success(
        self,
        api_client,
        fixture__cart,
        item_data,
    ):
        assert Cart.objects.get(id=fixture__cart.id)
        assert Cart.objects.all().count() == 1
        assert not Item.objects.filter().first()

        response_create = api_client().post(
            reverse.reverse("item"),
            data=item_data,
            format="json",
        )

        expected_db_item = item_data[0] | {"cart": fixture__cart.id}

        assert response_create.status_code == status.HTTP_204_NO_CONTENT
        assert Cart.objects.all().count() == 2
        assert not Item.objects.filter(**expected_db_item).first()


    @pytest.mark.django_db
    @pytest.mark.parametrize(
        "item_data",
        ([{"name": "jeans", "value": 15}], [{"name": "hoodie", "value": 500}]),
    )
    def test__integration__add_item_to_cart__cart_does_exist__cookie__success(
        self,
        api_client,
        fixture__cart,
        item_data,
    ):
        assert Cart.objects.get(id=fixture__cart.id)
        assert Cart.objects.all().count() == 1
        assert not Item.objects.filter().first()

        api_client = api_client()
        api_client.cookies = SimpleCookie({settings.CART_COOKIE_KEY: fixture__cart.id})
        response_create = api_client.post(
            reverse.reverse("item"),
            data=item_data,
            format="json",
        )

        expected_db_item = item_data[0] | {"cart": fixture__cart.id}

        assert response_create.status_code == status.HTTP_204_NO_CONTENT
        assert Cart.objects.all().count() == 1
        assert Item.objects.get(**expected_db_item)
