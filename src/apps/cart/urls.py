from django.urls import path

from apps.cart.views import ItemCreateAPIView


urlpatterns = [
    path("item/", ItemCreateAPIView.as_view(), name="item"),
]
