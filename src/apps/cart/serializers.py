from rest_framework import serializers

from apps.cart.models import Item


class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Item
        fields = ("id", "name", "value")
        read_only_fields = ("id",)
