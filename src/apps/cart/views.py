from datetime import timedelta

from django.conf import settings
from django.http import HttpResponse
from django.utils import timezone
from rest_framework import generics, status

from apps.cart.models import Cart, Item
from apps.cart.serializers import ItemSerializer


""" 
I followed the task description, but normally I would have an additional, intermediate model between Cart and Item to
connect them in M2M relationship, and the ItemCreateAPIView accept not {"name": str, "value": int} object(s), but rather
a previously fetched Item.id(s). In such scenario Items would be pre-defined by admin, returned as a list in a separate
view, from which user would be able to choose before passing them back from front-end to back-end's.
"""


class ItemCreateAPIView(generics.CreateAPIView):
    serializer_class = ItemSerializer(many=True)  # we could consider optimizing by sending multiple Item objects (or
    # Item ids) to be placed in the Cart.

    def create(self, request, *args, **kwargs):
        cart_id = request.COOKIES.get(settings.CART_COOKIE_KEY)
        cart = Cart.objects.filter(id=cart_id).first()  # At the moment there is no n+1 problem with cart.items as it's
        # not being called anywhere, but normally I would use .prefetch_related() controlled further by Prefetch object
        # if needed.
        if not cart:
            cart = Cart.objects.create()

        items_to_create = list()
        for item in request.data:
            items_to_create.append(Item(cart_id=cart.id, **item))
        Item.objects.bulk_create(items_to_create)  # if it were many-to-many relationship we would instead be using
        # cart.items.add([ID, ID, ID]), which similarly would cost only one additional database query

        response = HttpResponse(status=status.HTTP_204_NO_CONTENT)
        response.set_cookie(
            key=settings.CART_COOKIE_KEY, value=str(cart.id), expires=timezone.now()+timedelta(hours=72)
        )

        return response
