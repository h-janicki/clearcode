# Generated by Django 4.1 on 2023-01-27 19:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("cart", "0001_initial"),
    ]

    operations = [
        migrations.AlterField(
            model_name="item",
            name="name",
            field=models.CharField(max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name="item",
            name="value",
            field=models.IntegerField(null=True),
        ),
        migrations.AddIndex(
            model_name="item",
            index=models.Index(fields=["cart"], name="cart_item_cart_id_305f60_idx"),
        ),
    ]
