# ClearCode technical evaluation task

## Local setup:

1. Install Docker. If you are using MS Windows, don't forget to install WSL2, Ubuntu.
2. Make sure the Docker engine is running.
3. Build the app by going to project root folder and typing in:
    ```
    docker-compose build
    ```
4. Apply django models to database by running:
    ```
    docker-compose run -u root django python src/manage.py migrate
    ```
5. To start the back-end once it's built type in:
    ```
    docker-compose up
    ```

## Administration:

1. Create superuser by running command:
   ```
   docker-compose run -u root django python src/manage.py createsuperuser
   ```
2. Access the admin panel at http://localhost:8000/admin
3. Login with the newly created credentials

## Pytest:
Run tests
```
docker-compose run -u root django pytest
```
